package cn.tedu.boot12.controller;

import cn.tedu.boot12.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {
    @RequestMapping("/reg")
    @ResponseBody
    public String reg(User user){

        return "我叫"+user.getNick()+" 注册完成!";
    }
}
