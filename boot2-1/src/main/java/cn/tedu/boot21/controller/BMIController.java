package cn.tedu.boot21.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BMIController {
    @RequestMapping("/bmi")
    @ResponseBody//以返回值的方式给客户端响应数据
    public String bmi(float h,float w){
        System.out.println("h = " + h + ", w = " + w);
        //计算bmi
        float bmi = w/(h*h);
        if (bmi<18.5){
            return "兄弟你瘦了!";
        }else if(bmi<24){
            return "身体倍儿棒!";
        }else if(bmi<28){
            return "微微胖";
        }
        return "兄弟该锻炼起来了!";
    }
}
