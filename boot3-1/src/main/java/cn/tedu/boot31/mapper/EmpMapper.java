package cn.tedu.boot31.mapper;

import cn.tedu.boot31.entity.Emp;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper //告诉编译器当前接口是一个映射接口 负责为Mybatis框架服务
public interface EmpMapper {
    //#{变量}会自动从方法的参数列表中找到对应的数据,如果找不到
    //会自动查看已有对象里面是否包含同名的属性
    @Insert("insert into emp values(null,#{name},#{salary},#{job})")
    void insert(Emp emp);
    //MyBaitis框架会自动根据上面两行代码生成方法的实现(方法里面写的是jdbc代码)

    @Select("select id,name,salary,job from emp")
    List<Emp> selectAll();

    @Delete("delete from emp where id=#{id}")
    int delete(int id);

    @Update("update emp set name=#{name}," +
            "salary=#{salary},job=#{job} where id=#{id}")
    void update(Emp emp);

}
