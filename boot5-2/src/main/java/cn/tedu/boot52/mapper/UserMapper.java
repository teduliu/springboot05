package cn.tedu.boot52.mapper;

import cn.tedu.boot52.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
    //创建查询用户名是否存在的方法
    @Select("select id,username,password,nick from user " +
            "where username=#{username}")
    User selectByUsername(String username);
    //创建注册用户的方法
    @Insert("insert into user values" +
            "(null,#{username},#{password},#{nick},null)")
    void insert(User user);


}
