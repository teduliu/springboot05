package cn.tedu.boot52.controller;

import cn.tedu.boot52.entity.User;
import cn.tedu.boot52.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
public class UserController {
    @Autowired(required = false)
    UserMapper mapper;

    @RequestMapping("/reg")
    public int reg(User user){
        //查询是否存在用户输入的用户名
        User u = mapper.selectByUsername(user.getUsername());
        if (u!=null){
            return 2;//用户名已存在
        }
        mapper.insert(user);
        return 1;//注册成功!
    }
    @RequestMapping("/login")
    public int login(User user, HttpSession session){
        //通过用户输入的用户名得到数据库中的用户信息
        User u = mapper.selectByUsername(user.getUsername());
        if (u!=null){//查询到了
            if (u.getPassword().equals(user.getPassword())){
                //u代表当前登录的用户对象
                //登录成功时 把当前登录的用户对象保存到session里面
                session.setAttribute("user",u);
                return 1;//登录成功;
            }
            return 3;//密码错误
        }
        return 2;//用户名不存在
    }

    @RequestMapping("/checklogin")
    public boolean checkLogin(HttpSession session){
        User user = (User) session.getAttribute("user");
        //user为null说明没登录返回false 反之为true
        return user==null?false:true;
    }
    @RequestMapping("/logout")
    public void logout(HttpSession session){
        //删除session中的user对象 表示退出了登录
        session.removeAttribute("user");
    }


}
