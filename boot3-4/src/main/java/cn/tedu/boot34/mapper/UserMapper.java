package cn.tedu.boot34.mapper;

import cn.tedu.boot34.entity.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper {

    @Insert("insert into user values(null,#{username},#{password},#{nick})")
    void insert(User user);

    //返回值是User对象 因为查询到的用户信息是一个  如果是多个返回List<User>
    @Select("select id,username,password,nick from user where username=#{username}")
    User login(String username);

    @Select("select id,username,password,nick from user")
    List<User> selectAll();

    @Delete("delete from user where id=#{id}")
    void delete(int id);
}
