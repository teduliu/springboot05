package cn.tedu.boot33.controller;

import cn.tedu.boot33.entity.Emp;
import cn.tedu.boot33.mapper.EmpMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class EmpController {
    @Autowired(required = false)
    EmpMapper mapper;

    @RequestMapping("/add")
    public String add(Emp emp){
        mapper.insert(emp);
        return "添加完成!";
    }

    @RequestMapping("/select")
    public String select(){

        List<Emp> list = mapper.selectAll();
        //将集合里面的数据装进html标签中 响应给客户端

        String html = "<table border='1'>";
        html+="<caption>员工列表</caption>";
        html+="<tr><th>id</th><th>姓名</th><th>工资</th><th>工作</th><th>操作</th></tr>";
        //遍历集合
        for (Emp e:list) {
            html+="<tr>";
            html+="<td>"+e.getId()+"</td>";
            html+="<td>"+e.getName()+"</td>";
            html+="<td>"+e.getSalary()+"</td>";
            html+="<td>"+e.getJob()+"</td>";
            html+="<td><a href='/delete?id="+e.getId()+"'>删除</a></td>";
            html+="</tr>";
        }
        html+="</table>";
        return html;
    }

    @RequestMapping("/delete")
    public void delete(int id, HttpServletResponse response) throws IOException {
        mapper.delete(id);
        //重定向到列表页面
        response.sendRedirect("/select");
    }

    @RequestMapping("/update")
    public void update(Emp emp,HttpServletResponse response) throws IOException {
        mapper.update(emp);
        //重定向到列表页面
        response.sendRedirect("/select");
    }


}
