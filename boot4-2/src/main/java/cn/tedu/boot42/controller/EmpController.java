package cn.tedu.boot42.controller;

import cn.tedu.boot42.entity.Emp;
import cn.tedu.boot42.mapper.EmpMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmpController {

    @Autowired(required = false)
    EmpMapper mapper;

    @RequestMapping("/add")
    public void add(Emp emp){
        mapper.insert(emp);
    }


    @RequestMapping("/select")
    public List<Emp> select(){
        //把从数据库中查询到的数据 直接响应给客户端
        return mapper.selectAll();
    }
    //此时给客户端最终响应的数据也是一个字符串,SpringMVC框架当看到返回值为List集合
    //或对象时,会自动将其转成json格式的字符串

    @RequestMapping("/delete")
    public void delete(int id){
        mapper.delete(id);
    }

    @RequestMapping("/selectbyid")
    public Emp selectById(int id){
        //通过员工id查询员工的详细信息
        return mapper.selectById(id);
    }

    @RequestMapping("/update")
    public void update(Emp emp){
        mapper.update(emp);
    }
}
