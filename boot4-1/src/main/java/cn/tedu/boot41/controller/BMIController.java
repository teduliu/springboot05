package cn.tedu.boot41.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BMIController {

    @RequestMapping("/bmi")
    public String bmi(float h,float w){
        //计算bmi的值
        float bmi = w/(h*h);
        return "bmi="+bmi;
    }
}
