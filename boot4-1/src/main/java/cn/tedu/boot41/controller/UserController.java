package cn.tedu.boot41.controller;

import cn.tedu.boot41.entity.User;
import cn.tedu.boot41.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired(required = false)
    UserMapper mapper;

    @RequestMapping("/check")
    public String check(String username,String password){
        System.out.println("username = " + username + ", password = " + password);
        //通过用户输入的用户名 查询对应的数据
        User user = mapper.selectByUsername(username);
        //判断是否查询到了数据
        return user==null?"用户名可用!":"用户名已存在!";
    }
}
