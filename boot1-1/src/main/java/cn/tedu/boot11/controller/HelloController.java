package cn.tedu.boot11.controller;

import cn.tedu.boot11.entity.Emp;
import cn.tedu.boot11.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Controller //告诉编译器当前类为一个控制器
public class HelloController {

    //当客户端向/hello地址发出请求时由此方法进行处理
    @RequestMapping("/hello")
    public void hello(String info,
                      HttpServletResponse response) throws IOException {
        //获取传递过来的参数
        //String info = request.getParameter("info");
        //request表示请求对象
        //response表示响应对象
        System.out.println("服务器接收到请求了!");
        //设置响应类型 告诉浏览器返回的内容是文本或html标签 字符集utf-8
        response.setContentType("text/html;charset=utf-8");
        //获取输出对象  异常抛出
        PrintWriter pw = response.getWriter();
        //给客户端输出数据
        pw.print("恭喜你!测试成功! info="+info);
        //关闭资源
        pw.close();


    }

    @RequestMapping("/login")
    public void login(String username,String password
            ,HttpServletResponse response) throws IOException {
        //设置响应类型
        response.setContentType("text/html;charset=utf-8");
        //获取输出对象  异常抛出
        PrintWriter pw = response.getWriter();
        //输出数据
        pw.print("接收到了username="+username+",password="+password);
        //关闭资源
        pw.close();
    }

    @RequestMapping("/reg")
    @ResponseBody //添加此注解后可以通过返回值的方式给客户端响应数据(SpringMVC提供)
    public String reg(User user){
        System.out.println("user = " + user);
        return "接收到请求了!";
    }
    @RequestMapping("/add")
    @ResponseBody
    public String add(Emp emp){
        return "我叫"+emp.getName()+"我的工作是"+emp.getJob()
                +",月薪"+emp.getSalary()+"元";
    }
}
