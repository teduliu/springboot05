package cn.tedu.tvrd.controller;

import cn.tedu.tvrd.entity.Product;
import cn.tedu.tvrd.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class ProductController {
    @Autowired(required = false)
    ProductMapper mapper;

    //读取配置文件中的数据
    @Value("${dirPath}")
    private String dirPath;


    @RequestMapping("/product/add")
    public void add(Product product, MultipartFile file) throws IOException {
        System.out.println("product = " + product + ", file = " + file);
        //得到唯一文件名
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        fileName = UUID.randomUUID()+suffix;
        //得到年月日相关的路径 E:/upload/2021/08/12/xxxx.jpg
        SimpleDateFormat format =
                new SimpleDateFormat("/yyyy/MM/dd/");
        //new Date() 代表当前时间  转换成上面指定的格式
        String datePath = format.format(new Date());
        File dirFile = new File(dirPath+datePath);
        //如果文件夹不存在 则创建  必须使用mkdirs()
        if (!dirFile.exists()){
            dirFile.mkdirs();//创建文件夹
        }
        //得到完整的文件路径
        String filePath = dirPath+datePath+fileName;
        //保存文件 异常抛出
        file.transferTo(new File(filePath));
        //把上传文件的路径设置给product对象   /2021/08/12/xxx.jpg
        product.setUrl(datePath+fileName);
        product.setCreated(new Date());
        //把product数据保存到数据库中
        mapper.insert(product);
    }

    @RequestMapping("/product/select")
    public List<Product> select(){
        return mapper.selectAll();
    }

    @RequestMapping("/product/delete")
    public void delete(int id){

        //通过id查询作品信息 E:/upload
        Product product = mapper.selectById(id);
        String filePath = dirPath+product.getUrl();
        //删除文件
        new File(filePath).delete();
        mapper.delete(id);
    }

    @RequestMapping("/product/select/view")
    public List<Product> selectView(){
        return mapper.selectView();
    }
    @RequestMapping("/product/select/like")
    public List<Product> selectLike(){
        return mapper.selectLike();
    }

    @RequestMapping("/product/selectbycid")
    public List<Product> selectByCid(int cid){
        return mapper.selectByCid(cid);
    }

    @RequestMapping("/product/selectbywd")
    public List<Product> selectByWd(String wd){
        return mapper.selectByWd(wd);
    }
    @RequestMapping("/product/selectbyid")
    public Product selectById(int id,HttpSession session){
        //取出浏览作品Id
        String viewId = (String) session.getAttribute("view"+id);
        if (viewId==null){//没有增加过才会进入此判断
            //让浏览量+1
            mapper.view(id);
            session.setAttribute("view"+id,"view"+id);
        }

        return mapper.selectById(id);
    }
    //点赞方法      作品id = 5
    @RequestMapping("/like")
    public int like(int id, HttpSession session){
        //取出点赞作品的id
        String likeId = (String) session.getAttribute("like"+id);
        if (likeId==null){//作品没点过赞
            session.setAttribute("like"+id,"like"+id);
            mapper.like(id);
            return 1;
        }
        return 2;

    }




}
