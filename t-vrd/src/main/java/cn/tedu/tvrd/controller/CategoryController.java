package cn.tedu.tvrd.controller;

import cn.tedu.tvrd.entity.Category;
import cn.tedu.tvrd.entity.Product;
import cn.tedu.tvrd.mapper.CategoryMapper;
import cn.tedu.tvrd.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CategoryController {
    @Autowired(required = false)
    CategoryMapper mapper;
    @Autowired(required = false)
    ProductMapper productMapper;

    //查询所有分类数据
    @RequestMapping("/category/select")
    public List<Category> select(){
        return mapper.selectAll();
    }

    @RequestMapping("/category/delete")
    public void delete(int id){

        mapper.delete(id);
        //删除和当前分类相关的作品 此时的id为分类id
        productMapper.deleteByCid(id);

    }
    @RequestMapping("/category/add")
    public void insert(Category category){
        mapper.insert(category);
    }


}
