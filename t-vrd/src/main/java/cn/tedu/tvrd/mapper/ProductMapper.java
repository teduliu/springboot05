package cn.tedu.tvrd.mapper;

import cn.tedu.tvrd.entity.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductMapper {
    @Insert("insert into product values(null,#{title},#{author}," +
            "#{intro},#{url},0,0,#{created},#{categoryId})")
    void insert(Product product);

    @Select("select id,title,author,url,view_count,like_count from product")
    @Result(column = "view_count",property = "viewCount")
    @Result(column = "like_count",property = "likeCount")
    List<Product> selectAll();

    @Delete("delete from product where id=#{id}")
    void delete(int id);

    @Select("select id,title,author,url from product" +
            " order by view_count desc limit 0,4")
    List<Product> selectView();

    @Select("select id,title,author,url from product" +
            " order by like_count desc limit 0,4")
    List<Product> selectLike();

    @Select("select id,title,author,url,view_count,like_count from product where category_id=#{cid}")
    @Result(column = "view_count",property = "viewCount")
    @Result(column = "like_count",property = "likeCount")
    List<Product> selectByCid(int cid);

    @Select("select id,title,author,url,view_count,like_count from product " +
            "where title like concat('%',#{wd},'%')")
    @Result(column = "view_count",property = "viewCount")
    @Result(column = "like_count",property = "likeCount")
    List<Product> selectByWd(String wd);

    @Select("select id,title,author,url,view_count,like_count,intro,created from product " +
            "where id=#{id}")
    @Result(column = "view_count",property = "viewCount")
    @Result(column = "like_count",property = "likeCount")
    Product selectById(int id);

    @Update("update product set like_count=like_count+1 where id=#{id}")
    void like(int id);

    @Update("update product set view_count=view_count+1 where id=#{id}")
    void view(int id);

    @Delete("delete from product where category_id=#{id}")
    void deleteByCid(int id);

}
