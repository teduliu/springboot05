package cn.tedu.tvrd.mapper;

import cn.tedu.tvrd.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
    @Select("select id,username,password from user where username=#{username}")
    User selectByUserName(String username);
}
