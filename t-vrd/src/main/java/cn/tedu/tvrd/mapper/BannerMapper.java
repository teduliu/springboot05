package cn.tedu.tvrd.mapper;

import cn.tedu.tvrd.entity.Banner;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BannerMapper {
    @Select("select id,url from banner")
    List<Banner> selectAll();

    @Delete("delete from banner where id=#{id}")
    void delete(int id);

    @Insert("insert into banner values(null,#{url})")
    void insert(Banner banner);
    @Select("select url from banner where id=#{id}")
    String selectFileName(int id);

}
