package cn.tedu.tvrd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

//Web容器组件扫描, 作用: 就是为了找到刚刚创建的MyFilter过滤器并加载
@ServletComponentScan
@SpringBootApplication
public class TVrdApplication {

    public static void main(String[] args) {
        SpringApplication.run(TVrdApplication.class, args);
    }

}
