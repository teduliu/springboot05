package cn.tedu.tvrd;

import cn.tedu.tvrd.entity.Product;
import cn.tedu.tvrd.entity.User;
import cn.tedu.tvrd.mapper.BannerMapper;
import cn.tedu.tvrd.mapper.CategoryMapper;
import cn.tedu.tvrd.mapper.ProductMapper;
import cn.tedu.tvrd.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
class TVrdApplicationTests {
    @Autowired(required = false)
    UserMapper mapper;

    @Test
    void contextLoads() {
        User u = mapper.selectByUserName("admin");
        System.out.println(u);
    }

    @Autowired(required = false)
    CategoryMapper categoryMapper;

    @Test
    void test01(){
        System.out.println(categoryMapper.selectAll());
    }

    @Autowired(required = false)
    BannerMapper bannerMapper;
    @Test
    void test02(){
        System.out.println(bannerMapper.selectAll());
    }

    @Autowired(required = false)
    ProductMapper productMapper;
    @Test
    void test03(){
        Product p = new Product();
        p.setTitle("标题");
        p.setAuthor("作者");
        p.setIntro("介绍");
        p.setUrl("images/a.jpg");
        p.setCreated(new Date());
        p.setCategoryId(1);
        productMapper.insert(p);
    }
    @Test
    void test04(){
        System.out.println(productMapper.selectAll());
    }
    @Test
    void test05(){
        System.out.println(productMapper.selectView());
        System.out.println(productMapper.selectLike());
    }
    @Test
    void test06(){
        System.out.println(productMapper.selectByWd("天气"));
    }

    //此注解会自动找到application.properties配置文件中的name对应的值
    //赋值给此变量
    @Value("${name}")
    private String name;

    @Test
    void test07(){
        System.out.println(name);
    }
}
