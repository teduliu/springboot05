package cn.tedu.boot32;

import cn.tedu.boot32.entity.Hero;
import cn.tedu.boot32.entity.Item;
import cn.tedu.boot32.mapper.HeroMapper;
import cn.tedu.boot32.mapper.ItemMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Boot32ApplicationTests {
    //required=false 作用是告诉idea编译器 这个变量装配不成功也没关系,
    //但是实际上正确操作的情况下不存在装配不成功的问题, 只是一个误提示.
    @Autowired(required = false)
    HeroMapper mapper;

    @Test
    void contextLoads() {
        Hero hero = new Hero();
        hero.setName("黄忠");
        hero.setType("射手");
        mapper.insert(hero);
        System.out.println("添加完成!");
    }
    @Test
    void select(){
        System.out.println(mapper.selectAll());
    }
    @Test
    void update(){
        Hero hero = new Hero();
        hero.setId(1);
        hero.setName("孙尚香");
        hero.setType("刘备媳妇儿");
        mapper.update(hero);
        System.out.println("修改完成!");
    }
    @Test
    void delete(){
        mapper.delete(1);
    }

    //把ItemMapper装配进来
    @Autowired(required = false)
    ItemMapper itemMapper;

    @Test
    void insert(){
        Item item = new Item();
        item.setTitle("realme手机");
        item.setPrice(2000);
        item.setCategoryId(5);
        item.setUserId(10);
        itemMapper.insert(item);
    }
    @Test
    void seleteAll(){
        System.out.println(itemMapper.selectAll());
    }



}
