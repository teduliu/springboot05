package cn.tedu.boot32.mapper;

import cn.tedu.boot32.entity.Item;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ItemMapper {
    @Insert("insert into item values(null,#{title},#{price}," +
            "#{categoryId},#{userId})")
    void insert(Item item);

    //通过Result注解设置字段和对象中属性的对应关系(如果名字一致的不需要配置)
    @Select("select id,title,price,category_id,user_id from item")
    @Result(column = "category_id",property = "categoryId")
    @Result(column = "user_id",property = "userId")
    List<Item> selectAll();








}
