package cn.tedu.boot32.mapper;

import cn.tedu.boot32.entity.Hero;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface HeroMapper {
    @Insert("insert into hero values(null,#{name},#{type})")
    void insert(Hero hero);
    @Delete("delete from hero where id=#{id}")
    void delete(int id);
    @Update("update hero set name=#{name},type=#{type} where id=#{id}")
    void update(Hero hero);
    @Select("select id,name,type from hero")
    List<Hero>selectAll();
}
